<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){
        // $products = Product::all();
        // $products = Product::where('name', 'like', '%' . $request->search .'%')->get();

        //Startando a query builder do Laragon
        $products = Product::query();

        //Com o query builder, podemos concatenar o when, where, etc.
        $products->when($request->search, function($query, $vl){
            $query->where('name', 'like', '%'. $vl .'%');
        });
        // $product->where();
        // $product->where();

        // if($request->search){
            // $product->where();
        // }

        $products = $products->get();

        return view('home', [
            'products' => $products
        ]);
    }
}
