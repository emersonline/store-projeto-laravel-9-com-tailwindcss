<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Redirect;
class AdminProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        //Compact dá um match na variável com o atributo definindo um array
        return view('admin.products', compact('products'));
    }

    //Mostrar página de criar
    public function create()
    {
        return view('admin.product_create');
    }

    //Mostrar a página de editar
    public function edit(Product  $product)
    {
        return view('admin.product_edit', [
            'product' => $product
        ]);
    }

    //Recebe requisição para dar update PUT
    public function update(Product $product, ProductStoreRequest $request)
    {
        $input = $request->validated();

        //Envio de imagem
        if(!empty($input['cover']) && $input['cover']->isValid()){
            Storage::delete($product->cover ?? '');
            $file = $input['cover'];
            $path = $file->store('products');
            $input['cover'] = $path;
        }

        $product->fill($input);
        $product->save();
        return Redirect::route('admin.products');

    }

    //Recebe a requisição de criar POST
    public function store(ProductStoreRequest $request)
    {
        $input = $request->validated();
        $input['slug'] = Str::slug($input['name']);

         //Envio de imagem
        if(!empty($input['cover']) && $input['cover']->isValid()){
            $file = $input['cover'];
            $path = $file->store('products');
            $input['cover'] = $path;
        }

        Product::create($input);

        return Redirect::route('admin.products');
    }

    public function destroy(Product $product){
        $product->delete();
        Storage::delete($product->cover ?? '');

        return Redirect::route('admin.products');
    }

    public function destroyImage(Product $product){
        Storage::delete($product->cover ?? '');
        $product->cover = null;
        $product->save();

        return Redirect::back();
    }
}
